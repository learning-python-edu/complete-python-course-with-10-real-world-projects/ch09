def standard_for_loop():
    temps = [221, 234, 340, 230]

    new_temps = []
    for temp in temps:
        new_temps.append(temp / 10)

    print(new_temps)


def for_comprehension():
    temps = [221, 234, 340, 230]

    new_temps = [temp / 10 for temp in temps]

    print(new_temps)


def list_comprehension_with_if():
    temps = [221, 234, 340, -9999, 230]

    new_temps = [temp / 10 for temp in temps if temp != -9999]

    print(new_temps)


def list_comprehension_with_if_else():
    temps = [221, 234, 340, -9999, 230]

    new_temps = [temp / 10 if temp != -9999 else 0 for temp in temps]

    print(new_temps)


if __name__ == '__main__':
    standard_for_loop()
    for_comprehension()
    list_comprehension_with_if()
    list_comprehension_with_if_else()
